﻿:- use_module(library(lists)).
:- ['donneesTP2.pl'].

%SETTERS

modifierBudgetFilm(IdFilm, Salaires) :- film(IdFilm,Titre,Type,Realisateur,Producteur,Cout,Duree,Nba,Budget), NBudget = Salaires, NCout = Budget - NBudget, retract(film(IdFilm,Titre,Type,Realisateur,Producteur,Cout,Duree,Nba,Budget)), assert(film(IdFilm,Titre,Type,Realisateur,Producteur,NCout,Duree,Nba,NBudget)).

affectationMaisonFilm(NomMaison, IdFilm) :- film(IdFilm,Titre,Type,Realisateur,Producteur,Cout,Duree,Nba,Budget), retract(film(IdFilm,Titre,Type,Realisateur,Producteur,Cout,Duree,Nba,Budget)), assert(film(IdFilm,Titre,Type,Realisateur,NomMaison,Cout,Duree,Nba,Budget)).

diminuerBudgetMaison(NomMaison, Cout) :- maison(NomMaison, B), NB is (B - Cout), retract(maison(NomMaison, B)), assert(maison(NomMaison, NB)).

ajouterFilm(IdCinema, IdFilm, PrixEntree) :- cinema(IdCinema, N, LFilms), retract(cinema(IdCinema, N, LFilms)), append(LFilms, [(IdFilm, 0, PrixEntree)], LFilms2), assert(cinema(IdCinema, N, LFilms2)).


%FONCTIONS CADRE

%Controle l'affection des roles d'une liste d'acteurs a un film, change les joueDans ainsi que le budget et le cout du film.
doAffectationDesRoles(_, La) :- La = pasAssezDacteur, !, fail.
doAffectationDesRoles(IdFilm, La) :- film(IdFilm,_,_,_,_,_,_,_,B), totalSalaireMin(La, Total), Total =< B, acteurJoueDansFilm(La, IdFilm), modifierBudgetFilm(IdFilm, Total).

%unifie les films et acteurs dans les joueDans

%verifie la validite d'un film avec les conditions standards.
validerFilm(IdFilm) :- film(IdFilm,_,_,R,P,_,_,Na,_), R = pasDeRealisateur, P = pasDeProducteur, listeActeurs(L), length(L, Ll), Na > Ll.

sansDoublons(L, R) :- list_to_set(L, R). 

% Determine si le film respecte toute les contraintes de l'acteur donne. 
estAdmissible(F,A) :- call(A, film(F,_,_,_,_,_,_,_,_)).

estAttribue(IdFilm) :- findall(X, joueDans(X, IdFilm), La), length(La, NbActeur), film(IdFilm,_,_,_,_,_,_,NbActRequis,_), NbActeur = NbActRequis.

% Retoune la liste des N premiers elements de la liste passe en parametre. 
prendN(N, _, XS) :- N =< 0, !, N =:= 0, XS = [].
prendN(_, [], []).
prendN(N, [X|XS], [X|YS]) :- M is N-1, prendN(M, XS, YS).

% Compte le nombre d'occurence de chaque acteur dans une liste.
compterActeur(La, Occ):- findall([X,L], (bagof(true,member(X,La),Xs), length(Xs,L)), Occ).

% Retourne la liste contenant tous les ID des cinemas.
listeCinemas(L) :- findall(X, cinema(X,_,_), L).
/* 0) Le predicat listeFilms(L) lie L à la liste (comportant les identifiants) de tous les films. 
Exemple: 
        ?- listeFilms(ListeDesFilms).
        ListeDesFilms = [sica, nuit, coupe, wind, avengers, iron, sherlock, wind2].
*/
listeFilms(L) :- findall(X, film(X,_,_,_,_,_,_,_,_), L).

/* 
1) 0.25pt. Le predicat listeActeurs(L) unifie L à la liste (comportant les identifiants) de tous les acteurs. 
*/

listeActeurs(L) :- findall(ID, acteur(_,_,_,_,ID), L).

/* 2) 0.25pt. Le predicat experience(IdAct,Annee,Ne) unifie Ne au nombre d'années d'expérience à l'année Annee, de l'acteur dont l"identifiant est IdAct. 
precondition: Annee doit être définie. 
*/ 

experience(IdAct, Annee, Ne) :- integer(Annee), acteur(_,_,_,date(Adebut,_,_),IdAct), Ne is Annee - Adebut. 

/* 
3) 0.75pt. Le predicat filtreCritere unifie ActId à l'identifiant du premier acteur qui verifie tous les criteres de Lc. 
precondition: Lc doit etre defini. 
*/

filtreCritere([],_).
filtreCritere([C|CS], IdAct) :- critere(C, acteur(_,_,_,_,IdAct)), filtreCritere(CS, IdAct). 
filtreCritere(_, [], _).
filtreCritere(Lc, [A|_], IdAct) :- filtreCritere(Lc, A), IdAct = A, !.
filtreCritere(Lc, [_|AS], IdAct) :- filtreCritere(Lc, AS, IdAct).

/* 
4) 0.75pt. Le predicat totalSalaireMin(LActeur,Total) calcule la somme des salaires minimuns exigés par les acteurs dont la liste (des identifiants) est spécifiée. 
*/

totalSalaireMin([], 0). 
totalSalaireMin([IdAct|AS],Total) :- acteur(_,_,RevAct,_,IdAct), totalSalaireMin(AS, TT), Total is TT+RevAct. 

/* 
5a) 0.75pt. Le prédicat selectionActeursCriteres(Lcriteres,Lacteurs) unifie Lacteurs à la liste formée des identifiants des acteurs qui satisfont tous les critères de Lcriteres.
precondition: la liste de criteres doit être définie. 
*/

selectionActeursCriteres([],L) :- !, listeActeurs(L).  
selectionActeursCriteres(Lcriteres,Lacteurs) :- findall(IdAct, filtreCritere(Lcriteres, IdAct), Lacteurs).

/* 
5b) 1pt. Le prédicat selectionActeursCriteresNouvelle(Lcriteres,Lacteurs,LChoisis) unifie LChoisis à la liste formée des identifiants des acteurs sélectionnés parmi les acteurs dans Lacteurs selon 
le principe suivant (jusqu'à concurrence de N acteurs, N correspondant au nombre de critères dans LCriteres: le premier acteur qui satisfait le premier critere de Lcriteres, le premier acteur 
non encore sélectionné et qui satisfait le deuxième critère etc.	
precondition: la liste de criteres (Lcriteres) et la liste des acteurs contenant leurs idenfiants (Lacteurs) doivent être définies. 
)
*/

selectionActeursCriteresNouvelle(Lc, La, LChoisis) :- is_list(Lc), is_list(La), LRetour = [], selectionActeursCriteresNouvelle(Lc, La, LChoisis, LRetour).
selectionActeursCriteresNouvelle(_, [], LChoisis, Retour) :- !, LChoisis = Retour.
selectionActeursCriteresNouvelle([], _, LChoisis, Retour) :- !, LChoisis = Retour.
selectionActeursCriteresNouvelle([C|CS], La, LChoisis, Retour) :- filtreCritere([C], La, ActId), delete(La, ActId, La1), nonvar(ActId), append(Retour, [ActId], Retour2), selectionActeursCriteresNouvelle(CS, La1, LChoisis, Retour2).

/* 
6) 1pt. Le prédicat filmsAdmissibles(ActId,LFilms) unifie LIdFilms à la liste des films (identifiants) satisfaisant les restrictions de l'acteur ActId. 
*/

%filmsAdmissibles(ActId,LFilms) :- findall(IdFilm, ActId(film(IdFilm,_,_,_,_,_,_,_,_)),LFilms). 
filmsAdmissibles(ActId,LFilms) :- findall(IdFilm, call(ActId, film(IdFilm,_,_,_,_,_,_,_,_)) ,LFilmsTemp), sansDoublons(LFilmsTemp, LFilms).  

/* 
7a) 1pt. Le prédicat selectionActeursFilm(IdFilm,Lacteurs) unifie Lacteurs à la liste formée des identifiants d'acteurs pour lesquels le film de d'identifiant IdFilm satisfait les restrictions.
préconditions: IdFilm doit être défini 
*/

selectionActeursFilm(IdFilm,Lacteurs):- listeActeurs(L), include(estAdmissible(IdFilm),L, Lacteurs). 

/*
7b) 1pt. Le prédicat selectionNActeursFilm2ActeursFilm2(IdFilm,Lacteurs) unifie Lacteurs à la liste formée des identifiants d'acteurs pour lesquels le film de d'identifiant IdFilm satisfait les restrictions.
          Si le nombre total des acteurs qualifiés est inférieur au nombre d'acteurs du film, la liste retournée (Lacteurs) devra contenir l'atome pasAssezDacteur.
préconditions: IdFilm doit être défini 
*/

selectionNActeursFilm2(IdFilm,Lacteurs) :- selectionActeursFilm(IdFilm,LTemp), length(LTemp, NbAct), film(IdFilm,_,_,_,_,_,_,NbActRequis,_), NbAct >= NbActRequis, !, prendN(NbActRequis, LTemp, Lacteurs). 
selectionNActeursFilm2(_,[pasAssezDacteur]).  

% test7b2 ???????????????

/* 
8) 1pt. Le prédicat acteurJoueDansFilm(Lacteurs, IdFilm) ajoute dans la base de faits tous les acteurs (identifiants) jouant dans le film de titre spécifié (IdFilm) 
*/
acteurJoueDansFilm([], _).
acteurJoueDansFilm([A|AS], IdFilm) :- assert(joueDans(A,IdFilm)), acteurJoueDansFilm(AS, IdFilm). 


/* 
9a) 1pt. Le prédicat affectationDesRolesSansCriteres(IdFilm) a pour but de distribuer les rôles à une liste d'acteurs pouvant jouer dans le film identifié par IdFilm (puisque
le film satisfait à ses restrictions). Les N premiers acteurs dont les restructions sont respectées par le film (N correspondant au nombre de rôles du film), sont ajoutés dans
dans la base de faits par des prédicats "joueDans".
Ce prédicat modifie le fait film correspondant à IdFilm par destruction et remplacement par un nouveau fait film égal à l'ancien mais dont le budget a été remplacé par la somme des salaires minimums des acteurs choisis et son coût a été diminué de la différence entre le budget initial et le nouveau budget.
Ce prédicat complète la base de faits joueDans(IdActeur, IdFilm) en fonction des N acteurs sélectionnés et dont la somme des salaires minimums est inférieure ou égale au budget (salarial) du film. Le prédicat doit envisager toutes les combinaisons possibles des N acteurs tirés de la base de faits acteur 
Le prédicat échoue et ne modifie rien si une des conditions suivantes est vérifiée (dans l'ordre):
  0) les rôles ont déjà été distribués por ce film V
  1) le réalisateur du film est pasDeRealisateur v
  2) le producteur du film est pasDeProducteur v
  3) s'il n'y a pas assez d'acteurs, v
  4) si le budget du film est insuffisant. v
précondition: L'identifiant du film doit être défini. v
*/

affectationDesRolesSansCriteres(IdFilm) :- not(atom(IdFilm)), !, fail.
affectationDesRolesSansCriteres(IdFilm) :- estAttribue(IdFilm), !, fail.
affectationDesRolesSansCriteres(IdFilm) :- validerFilm(IdFilm), !, fail.
affectationDesRolesSansCriteres(IdFilm) :- findall(X, joueDans(X, IdFilm), La), affectationDesRolesSansCriteres(IdFilm, La).

affectationDesRolesSansCriteres(IdFilm, []) :- !, selectionNActeursFilm2(IdFilm, La), not(La = pasAssezDacteur), doAffectationDesRoles(IdFilm, La).

affectationDesRolesSansCriteres(IdFilm, ActSelect) :- !, selectionNActeursFilm2(IdFilm, La), not(La = pasAssezDacteur), subtract(La, ActSelect, La2),
                                                      length(ActSelect, NbActSelect), film(IdFilm,_,_,_,_,_,_,NbActRequis,_), N = NbActRequis - NbActSelect, prendN(N, La2, La3),
                                                      length(La3, NbActSelect2), NbActRequis =:= NbActSelect + NbActSelect2, append(ActSelect, La3, LaFinale), doAffectationDesRoles(IdFilm, LaFinale), !.
/*
9b) 1pt. Le prédicat affectationDesRolesCriteres(IdFilm,Lcriteres,LChoisis) unifie LChoisis à la liste d'acteurs satisfaisant aux critères de sélection du film, Ce film doit bien entendu satisfaire aux restrictions de 
chacun des acteurs candidat. 
Dans ce prédicat, IdFilm est un identifiant de film et Lcriteres est une liste de critères. 
Pour la satisfaction des critère, on retiendra toujours le premier acteur satisfaisant au 1er critère et on recommensera avec le même principe pour les autres acteurs et les critères restants.
Contrairement au prédicat affectationDesRolesSansCriteres défini à la question 9a, affectationDesRolesCriteres ne modifie pas ba base de faits et se contente de récupérer la liste des acteurs sélectionnés dans Lchoisis.
Le prédicat échoue
  1) si la liste des critère est vide,
  2) si le réalisateur du film est pasDeRealisateur,
  3) si le producteur du film est pasDeProducteur,
  4) s'il n'y a pas assez d'acteurs.
précondition: L'identifiant du film et la liste de critères doivent être définis.
Attention: Il est possible qu'il y ait moins de critère que d'acteurs admissibles. Dans ce cas, la liste des acteurs sélectionnés ne peut dépasser le nombre de critères dans Lcriteres.
           Le nombre maximum d'acteurs choisis est donc égal à la taille de la liste Lcriteres.
*/

affectationDesRolesCriteres(IdFilm,_,_) :- not(atom(IdFilm)), !, fail.
affectationDesRolesCriteres(_,LCriteres,_) :- not(is_list(LCriteres)), !, fail.
affectationDesRolesCriteres(_,[],_) :- !, fail.
affectationDesRolesCriteres(IdFilm,_,_) :- validerFilm(IdFilm), !, fail.
affectationDesRolesCriteres(IdFilm,LCriteres,LChoisis) :- selectionNActeursFilm2(IdFilm, La), not(La = pasAssezDacteur), selectionActeursCriteresNouvelle(LCriteres, La, LChoisis), length(LChoisis, NbActeur), length(LCriteres, NbActeur), !.

/*
10) 2pts. Le prédicat affectationDesRoles(IdFilm, Lcriteres) a pour but de distribuer les rôles à une liste d'acteurs pouvant jouer dans le film et satisfaisant
aux critères de sélection du film en ajoutant les acteurs choisis dans la base de faits "joueDans".
Dans ce prédicat, IdFilm est un identifiant de film et Lcriteres est une liste de critères. 
Pour la satisfaction des critère, on retiendra toujours le premier acteur satisfaisant au 1er critère et on recommensera avec le même principe pour les autres acteurs et les critères restants.
Ce prédicat modifie le fait film correspondant à IdFilm par destruction et remplacement par un nouveau fait film égal à l'ancien mais dont le budget a été remplacé par la somme des salaires minimums des acteurs choisis et son coût a été diminué de la différence entre le budget initial et le nouveau budget.
Ce prédicat complète la base de faits joueDans(IdActeurActeur, IdFilm) en fonction des n acteurs sélectionnés (où n est le nombre de rôles du film) qui satisfont tous les critères de Lcriteres, pour lesquels le film satisfait leur restrictions et dont la somme des salaires minimums est inférieure ou égale au budget du film. 
Le prédicat doit envisager toutes les combinaisons possibles des n acteurs tirés de la base de faits acteur.
Le prédicat échoue et ne modifie rien 
  1) si le réalisateur du film est pasDeRealisateur,
  2) si le producteur du film est pasDeProducteur,
  3) s'il n'y a pas assez d'acteurs,
  4) si le budget du film est insuffisant pour financer le salaire minimum de tous acteurs sélectionnés.
précondition: L'identifiant du film et la liste de critères doivent être définis.
Attention: 
1) Il est possible qu'il y ait moins de critère que d'acteurs admissibles. Dans ce cas, la liste des acteurs sélectionnés doit être complétée (si possible et à concurrence de nombre de rôles) selon le principe du prédicat affectationDesRolesSansCriteres(IdFilm) de la question 9a.
2) Si la liste Lcriteres est vide, c'est aussi le principe de affectationDesRolesSansCriteres(IdFilm) de la question 9a qui s'applique.
*/

affectationDesRoles(IdFilm, _) :- estAttribue(IdFilm), !, fail.
affectationDesRoles(IdFilm, []) :- !, affectationDesRolesSansCriteres(IdFilm).
affectationDesRoles(IdFilm, LCriteres) :- affectationDesRolesCriteres(IdFilm, LCriteres, LChoisis), !, affectationDesRolesSansCriteres(IdFilm, LChoisis).
affectationDesRoles(IdFilm, _) :- affectationDesRolesSansCriteres(IdFilm).

/* 11) 1,25 pts. Le prédicat produire(NomMaison,IdFilm) vérifie si la maison peut produire le film identifié. Il vérifie si le budget de la maison 
est supérieur au cout du film, si le réalisateur n'est pas pasDeRealisateur, et si le producteur n'est pas pasDeProducteur. Si la production est possible,
 on diminue le budget de la maison par le coût du film et on remplace le fait 'film' par un nouveau film égal à l'ancien sauf que la composante producteur 
 est égale à NomMaison. 
 Précondition: le nom de la maison et l'identifiant du film doivent être connus. Le prédicat doit échoué si la maison ne peut pas produire le film. 
*/

produire(NomMaison, _) :- not(atom(NomMaison)), !, fail.
produire(_, IdFilm) :- not(atom(IdFilm)), !, fail.
produire(NomMaison, IdFilm) :- maison(NomMaison, B), film(IdFilm,_,_,_,R,C,_,_,_), B>C, R = pasDeProducteur, diminuerBudgetMaison(NomMaison, C), affectationMaisonFilm(NomMaison, IdFilm).

							 
/* 12) 0.75pt. Le prédicat plusieursFilms(N,Lacteurs) unifie Acteurs à la liste des acteurs (comportant leurs NOMS), qui jouent dans au moins N films.
N doit être lié à une valeur au moment de la requête de résolution du but 
*/

acteurJoueNFilms(N, L) :- findall(X, joueDans(X, _), La), integer(N), compterActeur(La, Occ), include(isGTN(N), Occ, La2), flatten(La2, La3), remove_integers(La3, La4), !, L = La4.

isGTN(N, [_, Occ]) :- Occ >= N.

remove_integers([], []).
remove_integers([X|T], R) :- integer(X), remove_integers(T, R).
remove_integers([X|T], [Nom|R]) :- not(integer(X)), acteur(Nom,_,_,_,X), remove_integers(T, R).

plusieursFilms(N, Lacteurs) :- var(Lacteurs), !, acteurJoueNFilms(N, Lacteurs).
plusieursFilms(N, Lacteurs) :- !, acteurJoueNFilms(N, L), plusieursFilms_include(Lacteurs, L). 

plusieursFilms_include(_, []).
plusieursFilms_include(L, [X|XS]) :- member(X, L), plusieursFilms_include(L, XS).

/* 13) 1.25pt. Les films réalisés et produits doivent maintenant être distribués dans les cinémas. On vous demande définir le prédicat distribuerFilm(IdFilm,PrixEntree) qui envoie le film identifié par IdFilm à tous les cinémas en spécifiant le prix d'entrée suggéré. 
Ce prédicat doit modifier la base de connaissances en ajoutant le triplet  (IdFilm,0,PrixEntree) dans le répertoire de chacun des cinémas déjà existants.
 */

distribuerFilm(IdFilm,_) :- film(IdFilm,_,_,_,P,_,_,_,_), P = pasDeProducteur, !, fail.
distribuerFilm(IdFilm,PrixEntree) :- listeCinemas(L), distribuerFilm(IdFilm, PrixEntree, L).
distribuerFilm(_,_,[]) :- !.
distribuerFilm(IdFilm, PrixEntree, [C|CS]) :- ajouterFilm(C, IdFilm, PrixEntree), distribuerFilm(IdFilm, PrixEntree, CS).


 %TEST A REVIEW :104/105(Giulia se charge de mail le prof) 
